<?php

/**
 * @file
 * Displays a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   element.
 * - $head: Markup for the HEAD element (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the
 *   current path, whether the user is logged in, and so on.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled in
 *   theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been
 *   disabled in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been
 *   disabled.
 * - $primary_links (array): An array containing primary navigation links for
 *   the site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links
 *   for the site, if they have been configured.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $content: The main content of the current Drupal page.
 * - $right: The HTML for the right sidebar.
 * - $node: The node object, if there is an automatically-loaded node associated
 *   with the page, and the node ID is the second argument in the page's path
 *   (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic
 *   content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<div id="main" class="<?php print $classes; ?>"<?php print $attributes; ?>><!-- this encompasses the entire Web site -->
	<div id="header"><header>
		<div class="container">
			<div id="title">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
          </a>
        <?php endif; ?>
        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan">
            <?php if ($site_name): ?>
              <?php if ($title): ?>
                <div id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
                </div>
              <?php else: /* Use h1 when the content title is empty */ ?>
                <h1 id="site-name">
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
                </h1>
              <?php endif; ?>
            <?php endif; ?>
    
            <?php if ($site_slogan): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
			</div><!--#title-->
			<?php if ($main_menu || $secondary_menu): ?>
        <div id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";} if (!empty($secondary_menu)) {print " with-secondary";} ?>">
          <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
          <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
        </div>
      <?php endif; ?>
      
        <div id="header-region">
          <div id="header-image" class="container">
	           <img alt="Extrimity" src="<?php print drupal_get_path('theme', variable_get('theme_default', NULL)); ?>/images/default-header.png">
          </div>
        </div>
        <!--#header-image-->
			<div class="clear"></div>
		</div><!--.container-->
	</header></div><!--#header-->
	<div class="container">

		<?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
		<?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

	    <div id="content">
			<div id="content-inner" class="inner column center">

			<?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
			  <div id="content-header">

				<?php print $breadcrumb; ?>

				<?php if ($page['highlighted']): ?>
				  <div id="highlighted"><?php print render($page['highlighted']) ?></div>
				<?php endif; ?>

				<?php if ($title): ?>
				  <h1 class="title"><?php print $title; ?></h1>
				<?php endif; ?>

				<?php print render($title_suffix); ?>
				<?php print $messages; ?>
				<?php print render($page['help']); ?>

				<?php if ($tabs): ?>
				  <div class="tabs"><?php print render($tabs); ?></div>
				<?php endif; ?>

				<?php if ($action_links): ?>
				  <ul class="action-links"><?php print render($action_links); ?></ul>
				<?php endif; ?>
				
			  </div> <!-- /#content-header -->
			<?php endif; ?>

			<div id="content-area">
			  <?php print render($page['content']) ?>
			</div>

			<?php print $feed_icons; ?>

			</div>
		</div> <!-- /content-inner /content -->
 
	<?php if ($page['right']): ?>
	<div id="sidebar">
	<?php print render($page['right']); ?>
	</div><!--sidebar-->
	<?php endif; ?>

	<div class="clear"></div>
	</div><!--.container-->

<?php
/**
 * @file footer.php
 * A footer file converted from the WordPress Whiteboard theme framework.
 */
?>
	<div id="footer"><footer>
		<div class="container">
			<div id="footer-content">
        <?php // print $footer_message; ?>
        <?php if (!empty($footer)): print $footer; endif; ?>
				<div id="nav-footer" class="nav"><nav>
          <?php if (!empty($footer_menu)): print $footer_menu; endif; ?>
				</nav></div><!--#nav-footer-->
				<p class="clear"><a href="#main">Top</a></p>
				<p>Built using a Drupal port of the <a href="http://whiteboardframework.com/">Whiteboard Framework for Wordpress</a> <span class="amp">&amp;</span> <a href="http://lessframework.com">Less Framework</a>.</p><?php /* Whiteboard Framework is free to use. You are only required to keep a link in the CSS. We do not require a link on the site, though we do greatly appreciate it. Likewise, Less Framework is free to use. Links are not required on the website or in the CSS but are greatly appreciated. */ ?>
			</div><!--#footer-content-->
		</div><!--.container-->
	</footer></div><!--#footer-->
</div><!--#main-->

